# Awesome Exam

## Mysql
Please upload sql file from database-dump folder located at the root file

## Setup and Installation

Please open your terminal and go to test-child theme

```bash
cd /wp-content/themes/test-child/

```

and run this command to build your assets. Make sure you have gulp and bower install globally, if you already have gulp and bower globally you can skip the first command.


```bash
npm install -g gulp bower
npm install
bower install
gulp
gulp watch

```
