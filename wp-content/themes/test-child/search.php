<?php get_template_part('templates/page', 'header'); ?>

<section class="section section-result">
	<div class="container">

		<h3>Search Results: </h3>
		<?php if (!have_posts()) : ?>
		  <div class="alert alert-warning">
		    <?php _e('Sorry, no results were found.', 'sage'); ?>
		  </div>
		<?php endif; ?>
		
		
		<?php while (have_posts()) : the_post(); ?>
		  <?php get_template_part('templates/content', 'search'); ?>
		<?php endwhile; ?>
	</div>
</section>

<?php the_posts_navigation(); ?>
