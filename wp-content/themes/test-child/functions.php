<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/acf.php'  // ACF
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// Theme Customization //
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'  => 'Theme General Options',
        'menu_title'  => 'Theme Options',
        'menu_slug'   => 'theme-general-options',
        'capability'  => 'edit_posts',
        'redirect'    => false
    ));

}


/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'News', 'Post Type General Name', 'awesome' ),
        'singular_name'       => _x( 'News', 'Post Type Singular Name', 'awesome' ),
        'menu_name'           => __( 'News', 'awesome' ),
        'parent_item_colon'   => __( 'Parent News', 'awesome' ),
        'all_items'           => __( 'All News', 'awesome' ),
        'view_item'           => __( 'View News', 'awesome' ),
        'add_new_item'        => __( 'Add News', 'awesome' ),
        'add_new'             => __( 'Add New', 'awesome' ),
        'edit_item'           => __( 'Edit News', 'awesome' ),
        'update_item'         => __( 'Update News', 'awesome' ),
        'search_items'        => __( 'Search News', 'awesome' ),
        'not_found'           => __( 'Not Found', 'awesome' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'awesome' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'news', 'awesome' ),
        'description'         => __( 'News news and reviews', 'awesome' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'news', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );
 
function add_my_post_types_to_query( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'post', 'news' ) );
    return $query;
}

add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

/*
* Create a shortcode that will print Hello World
*/ 
function hello_shortcode( $atts ) {
   $a = shortcode_atts( array(
      'name' => 'world'
   ), $atts );
   return '<div class="test-shortcode"> Hello ' . $a['name'] . '! </div>';
}


add_shortcode( 'helloworld', 'hello_shortcode' );