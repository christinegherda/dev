<header>
  <div class="container">
    <nav class="navbar navbar-expand-lg">
      <?php if (get_field('brand_image', 'option')): ?>
        <a class="navbar-brand brand" href="<?php echo get_home_url(); ?>">
          <img src="<?=the_field('brand_image', 'option'); ?>" alt="<?=the_field('brand_name', 'option'); ?>">
          <?php if (get_field('brand_name', 'option')): ?>
            <span><?=the_field('brand_name', 'option'); ?></span>
          <?php endif ?>
        </a>
      <?php endif ?>
      <?php if (get_field('contact_number', 'option')): ?>
        <div class="contact-us ml-auto">
          <a href="tel:+<?=the_field('contact_number', 'option'); ?>">
            <span class="icon icon-phone d-sm-block d-md-none"></span>
            <span class="d-none d-sm-none d-md-block">+<?=the_field('contact_number', 'option'); ?></span>
          </a>
        </div>
      <?php endif ?>
    </nav>
  </div>
</header>