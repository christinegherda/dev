<?php use Roots\Sage\Titles; ?>

<div class="page-header">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-mobile">
			
			<div class="global-search-form">
				<?php dynamic_sidebar('sidebar-primary'); ?>
			</div>
	      <button class="navbar-toggler collapsed menu-toggle" onclick="openNav()">
	        <span class="icon-bar top-bar"></span>
	        <span class="icon-bar middle-bar"></span> 
	        <span class="icon-bar bottom-bar"></span>
	      </button>

		  <div class="navbar-collapse" id="navbarSupportedContent">
		      <?php
		      if (has_nav_menu('primary_navigation')) :
		        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav ml-auto', 'container' => 'ul']);
		      endif;
		      ?>
		  </div>
		</nav>
	</div>
</div>
