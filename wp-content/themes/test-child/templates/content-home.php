<div class="main-content">
	<?php get_template_part('templates/components/banner'); ?>
	<section class="section section-l1">
		<div class="container">
			<div class="row">
				<div class="content-about-us col-md-4 col-12">
					<?php if (get_field('about_title')): ?>
						<h3><?=the_field('about_title'); ?></h3>
					<?php endif ?>
					<?php if (get_field('about_description')): ?>
						<p>
							<?=the_field('about_description'); ?>
						</p>
					<?php endif ?>
				</div>
				<div class="content-news col-md-8 col-12">
					<?php get_template_part('templates/components/news'); ?>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-plugin-here">
		<div class="container">
			<?php the_content(); ?>
		</div>
	</section>
</div>