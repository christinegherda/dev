<footer class="content-info">
  <div class="container text-center">
  <?php if (get_field('brand_image', 'option')): ?>
    <a class="brand" href="<?php echo get_home_url(); ?>">
      <img src="<?=the_field('brand_image', 'option'); ?>" alt="<?=the_field('brand_name', 'option'); ?>">
      <?php if (get_field('brand_name', 'option')): ?>
        <span><?=the_field('brand_name', 'option'); ?></span>
      <?php endif ?>
    </a>
  <?php endif ?>
  </div>
</footer>
