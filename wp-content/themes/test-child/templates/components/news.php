<h3>Our News</h3>
<?php
$args = array(
  'post_type'   => 'news',
  'post_status' => 'publish',
  'posts_per_page' => 2
 );
 
$news = new WP_Query( $args );
if( $news->have_posts() ) :
  while( $news->have_posts() ) :  $news->the_post(); ?>
		<div class="news-item row">
			<div class="col-4">
				<div class="news-img-wrapper">
					<img src="<?php the_post_thumbnail_url('full'); ?>" alt="">
				</div>
			</div>
			<div class="col-8">
				<div class="news-details">
					<p class="date-published"><?php the_modified_date(); ?></p>
					<p class="description">
						<?=get_the_excerpt(); ?>
					</p>
				</div>
			</div>
		</div>
    <?php
  endwhile;
  wp_reset_postdata();
endif;
?>