<section class="section-banner">
	<div class="container">
	      <?php if( have_rows('banner_item') ): ?>
	        <div class="owl-carousel owl-theme banner">
	            <?php  while ( have_rows('banner_item') ) : the_row(); ?>
				    <div class="item">
				    	<div class="row">
				    		<div class="col-md-4 col-sm-4 col-12">
				    			<div class="banner-caption">
				    				<div class="date">
				    					<span class="icon icon-clock"></span>
				    					<?php the_sub_field('banner_date'); ?>
				    				</div>
				    				<div class="description">
				    					<?php the_sub_field('banner_title'); ?>
				    				</div>
				    				<a href="<?php the_sub_field('banner_link'); ?>" target="_blank" class="btn btn-green">Read More</a>
				    			</div>
				    		</div>
				    		<div class="col-md-8 col-sm-8 col-12">
				    			<div class="banner-img-wrapper">
				    				<img src="<?php the_sub_field('banner_image'); ?>" alt="">
				    			</div>
				    		</div>
				    	</div>
				    </div>


	             <?php endwhile; ?>
	        </div>
	      <?php  endif; ?>
	</div>
</section>