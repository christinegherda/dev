<section class="section section-page">
	<div class="container">
		<h3><?php the_title(); ?></h3>
		<?php the_content(); ?>
	</div>
</section>