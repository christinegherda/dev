<?php
/**
 * Plugin Name: Hello Plugin
 * Plugin URI: http://www.hello-plugin.com
 * Description: Plugin for my exam
 * Version: 1.0
 * Author: Christine G Herda
 */

add_action( 'the_content', 'hello_world' );

function hello_world ( $content ) {
    return $content .= '<p>Hello World!</p>';
}